'use strict';

const Gatherer = require('lighthouse').Gatherer;

class RequestTime extends Gatherer {
    afterPass(options) {
        const driver = options.driver;

        return driver.evaluateAsync('window.requestTime')
            .then(requestTime => {
                if (!requestTime) {

                    throw new Error('Unable to find request time metrics in page');
                }
                return requestTime;
            });
    }
}

module.exports = RequestTime;
