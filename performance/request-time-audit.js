'use strict';

const Audit = require('lighthouse').Audit;

const MAX_RESPONSE_TIME = 3000;

class LoadAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'request-time-audit',
            description: 'Measure request time',
            failureDescription: 'Request time slow to initialize',
            helpText: 'Used to measure time from navigationStart to when the response from request is done',
            requiredArtifacts: ['RequestTime']
        };
    }

    static audit(artifacts) {
        const requestTime = artifacts.RequestTime;

        const belowThreshold = requestTime <= MAX_RESPONSE_TIME;

        return {
            rawValue: requestTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadAudit;
